import requests
import time
from datetime import datetime
from insert import insertbit, insertet, insertcom, insertsol, insertcar, insertshi, createtable

a = 0
def listrequest(a):
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    print("A fecha y hora:", dt_string)	
    
    def requestbitcoin(now):
        r = requests.get("https://api.coinbase.com/v2/exchange-rates?currency=BTC")
        bitvalue = r.json() ['data'] ['rates'] ['EUR']
        print("El precio del Bitcoin es: " + bitvalue + "€")
        insertbit(bitvalue, now)

    def requestethereum(now):
        r = requests.get("https://api.coinbase.com/v2/exchange-rates?currency=ETH")
        etvalue = r.json() ['data'] ['rates'] ['EUR']
        print("El precio del Ethereum es: " + etvalue + "€")
        insertet(etvalue, now)

    def requestcompund(now):
        r = requests.get("https://api.coinbase.com/v2/exchange-rates?currency=COMP")
        comvalue = r.json() ['data'] ['rates'] ['EUR']
        print("El precio del Compound es: " + comvalue + "€") 
        insertcom(comvalue, now)

    def requestsolana(now):
        r = requests.get("https://api.coinbase.com/v2/exchange-rates?currency=SOL")
        solvalue = r.json() ['data'] ['rates'] ['EUR']
        print("El precio del Solana es: " + solvalue + "€")
        insertsol(solvalue, now)

    def requestcardano(now):
        r = requests.get("https://api.coinbase.com/v2/exchange-rates?currency=ADA")
        carvalue = r.json() ['data'] ['rates'] ['EUR']
        print("El precio del Cardano es: " + carvalue + "€")
        insertcar(carvalue, now)
    
    def requestshiba(now):
        r = requests.get("https://api.coinbase.com/v2/exchange-rates?currency=SHIB")
        shivalue = r.json() ['data'] ['rates'] ['EUR']
        print("El precio del Shiba es: " + shivalue + "€")
        insertshi(shivalue, now)

    requestbitcoin(now)
    requestethereum(now)
    requestcompund(now)
    requestsolana(now)
    requestcardano(now)
    requestshiba(now)

def printit(a):
    while a <= 50000:
        a = a+6
        listrequest(a)
        print(a)
        time.sleep(90.0)
    else:
        createtable()
        a = 0

printit(a)
