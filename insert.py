import mysql.connector
from mysql.connector import Error
from datetime import datetime


def insertbit(bitvalue, now):
    connection = mysql.connector.connect(host='localhost', port= 5555, database='analisis_coinbase', user='root', password='root')

    if connection.is_connected():
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)
    
    try:  
        #Execute SQL Query to insert record  
        cursor.execute("Insert into cotizacion values('BTC', 'Bitcoin', %s, %s);", (bitvalue, now))  
        connection.commit() # Commit is used for your changes in the database  
        print('Record inserted successfully...')   
    
    except mysql.connector.Error as error:
        print("Failed to insert {}".format(error))
    connection.close()#Connection
        
def insertet(etvalue, now):
    connection = mysql.connector.connect(host='localhost', port= 5555, database='analisis_coinbase', user='root', password='root')
        
    if connection.is_connected():
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)
    
    try: 
        cursor.execute("Insert into cotizacion values('ETH', 'Ethereum', %s, %s);", (etvalue, now))
        connection.commit()
        print('Record inserted successfully...')   
    
    except mysql.connector.Error as error:
        print("Failed to insert record into Laptop table {}".format(error))
    connection.close()

def insertcom(comvalue, now):
    connection = mysql.connector.connect(host='localhost', port= 5555, database='analisis_coinbase', user='root', password='root')
        
    if connection.is_connected():
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)
    
    try: 
        cursor.execute("Insert into cotizacion values('COMP', 'Compound', %s, %s);", (comvalue, now))
        connection.commit()
        print('Record inserted successfully...')   
    
    except mysql.connector.Error as error:
        print("Failed to insert record into Laptop table {}".format(error))
    connection.close()

def insertsol(solvalue, now):
    connection = mysql.connector.connect(host='localhost', port= 5555, database='analisis_coinbase', user='root', password='root')
        
    if connection.is_connected():
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)
    
    try: 
        cursor.execute("Insert into cotizacion values('SOL', 'Solana', %s, %s);", (solvalue, now))
        connection.commit()
        print('Record inserted successfully...')   
    
    except mysql.connector.Error as error:
        print("Failed to insert record into Laptop table {}".format(error))
    connection.close()

def insertcar(carvalue, now):
    connection = mysql.connector.connect(host='localhost', port= 5555, database='analisis_coinbase', user='root', password='root')
        
    if connection.is_connected():
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)
    
    try: 
        cursor.execute("Insert into cotizacion values('ADA', 'Cardano', %s, %s);", (carvalue, now))
        connection.commit()
        print('Record inserted successfully...')   
    
    except mysql.connector.Error as error:
        print("Failed to insert record into Laptop table {}".format(error))
    connection.close()

def insertshi(shivalue, now):
    connection = mysql.connector.connect(host='localhost', port= 5555, database='analisis_coinbase', user='root', password='root')
        
    if connection.is_connected():
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)
    
    try: 
        cursor.execute("Insert into cotizacion values('SHIB', 'Shiba', %s, %s);", (shivalue, now))
        connection.commit()
        print('Record inserted successfully...')   
    
    except mysql.connector.Error as error:
        print("Failed to insert record into Laptop table {}".format(error))
    connection.close()

def createtable():
    connection = mysql.connector.connect(host='localhost', port= 5555, database='analisis_coinbase', user='root', password='root')
    fecha = datetime.today().strftime('%Y%m%d')    
    cotizaciones = "Cotizaciones" + fecha

    if connection.is_connected():
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)


    try:
        cursor.execute("create table {tab} (IdCrypto varchar(10), NombreCrypto varchar(45), FactorCambio decimal(10,10), Fecha datetime);" .format(tab=cotizaciones))
        connection.commit()
        print('Tabla creada...')   
    
    except mysql.connector.Error as error:
        print("Failed to insert record into Laptop table {}".format(error))
    connection.close()

